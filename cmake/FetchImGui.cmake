message(STATUS "Fetching Dear ImGui")

include(FetchContent)

FetchContent_Declare(
    imgui_src
    GIT_REPOSITORY https://github.com/ocornut/imgui.git
    GIT_TAG "docking"
    GIT_SHALLOW TRUE
    GIT_PROGRESS TRUE
    SOURCE_DIR ${CMAKE_SOURCE_DIR}/lib/imgui

    USES_TERMINAL_DOWNLOAD TRUE
    USES_TERMINAL_BUILD TRUE
)

FetchContent_GetProperties(imgui_src)
if (NOT imgui_src_POPULATED)
    FetchContent_MakeAvailable(imgui_src)
    message(STATUS "imgui_src ${imgui_src_SOURCE_DIR} ${imgui_src_BINARY_DIR}")
endif()

