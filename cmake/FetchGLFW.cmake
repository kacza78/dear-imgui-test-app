message(STATUS "Fetching GLFW")

include(FetchContent)

FetchContent_Declare(
    glfw_src
    GIT_REPOSITORY https://github.com/glfw/glfw.git
    GIT_SHALLOW TRUE
    # GIT_TAG "3.3.7"
    GIT_PROGRESS TRUE
    # SOURCE_DIR ${CMAKE_SOURCE_DIR}/lib/glfw
    # BUILD_IN_SOURCE TRUE
    # BINARY_DIR ""# ${CMAKE_SOURCE_DIR}/lib/glfw/build/
    # CONFIGURE_COMMAND "mkdir -p ${CMAKE_CURRENT_SOURCE_DIR}/lib/glfw/build/ && cd ${CMAKE_CURRENT_SOURCE_DIR}/lib/glfw/build/ && cmake ${CMAKE_CURRENT_SOURCE_DIR}/lib/glfw/"
    # BUILD_COMMAND make

    # USES_TERMINAL_DOWNLOAD TRUE
    # USES_TERMINAL_BUILD TRUE

    # CMAKE_ARGS
    #     -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    #     -DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_CURRENT_BINARY_DIR}/glfw
)

FetchContent_GetProperties(glfw_src)
if (NOT glfw_src_POPULATED)
    FetchContent_MakeAvailable(glfw_src)
    message(STATUS "GLFW src fetched: ${glfw_src_SOURCE_DIR}, binary: ${glfw_src_BINARY_DIR}")

    set(BUILD_SHARED_LIBS OFF)
    set(GLFW_BUILD_EXAMPLES OFF CACHE INTERNAL "Build the GLFW example programs")
    set(GLFW_BUILD_TESTS OFF CACHE INTERNAL "Build the GLFW test programs")
    set(GLFW_BUILD_DOCS OFF CACHE INTERNAL "Build the GLFW documentation")
    set(GLFW_INSTALL OFF CACHE INTERNAL "Generate installation target")
endif()
