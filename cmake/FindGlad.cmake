
set(GLAD_INC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/lib/glad)

find_path (GLAD_INCLUDE_DIR
    NAMES glad.h
    HINTS ${GLAD_INC_DIR}
    PATH_SUFFIXES include
    NO_DEFAULT_PATH
)


include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
        REQUIRED_VARS GLAD_INCLUDE_DIR
)
