
set(GLFW_INC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/lib/glfw)

if(MSVC)
    set(GLFW_LIB_DIR ${GLFW_INC_DIR}/build)
else()
    set(GLFW_LIB_DIR ${GLFW_INC_DIR}/build/src)
endif()

find_path (GLFW_INCLUDE_DIR
    NAMES GLFW/glfw3.h
    HINTS ${GLFW_INC_DIR}
    PATH_SUFFIXES include
    NO_DEFAULT_PATH
    REQUIRED
)

find_library(GLFW_LIBRARY
    NAMES glfw3
    PATHS ${GLFW_LIB_DIR}
    NO_DEFAULT_PATH
    REQUIRED
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
        REQUIRED_VARS GLFW_INCLUDE_DIR GLFW_LIBRARY
)
