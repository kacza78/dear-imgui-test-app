#ifndef IMGUI_WRAPPER_HPP
#define IMGUI_WRAPPER_HPP

#include <memory>

class GLFWWrapper;
struct ImGuiContext;

class ImGuiWrapper
{
public:
    ImGuiWrapper();
    ~ImGuiWrapper();
    void loop();
private:
    std::unique_ptr<GLFWWrapper> glfwWrapper;
    ImGuiContext* ctx = nullptr;
};

#endif // IMGUI_WRAPPER_HPP