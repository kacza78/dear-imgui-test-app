#ifndef GLFW_WRAPPER_HPP
#define GLFW_WRAPPER_HPP

struct GLFWwindow;

class GLFWWrapper
{
public:
    GLFWWrapper();
    ~GLFWWrapper();

    void swapBuffers();

    bool shouldClose() const;
    bool getFramebufferSize(int &display_w, int &display_h) const;
    GLFWwindow* getGLFWWindow() const { return window; }

private:
    GLFWwindow* window;
};

#endif // GLFW_WRAPPER_HPP