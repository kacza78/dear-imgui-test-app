#include <iostream>

#include <glad.h>
#include <GLFW/glfw3.h>

#include "GLFWWrapper.hpp"


GLFWWrapper::GLFWWrapper()
{
    if(!glfwInit())
    {
        std::abort();
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    // Create window with graphics context
    window = glfwCreateWindow(1280, 720, "Dear ImGui GLFW+OpenGL3 example", NULL, NULL);
    if (!window)
    {
        std::cerr << "Failed creating glfw window\n";
        std::abort();
    }
    else
    {
        std::cout << "GLFW Window created\n";
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cerr << "Failed to initialize GLAD\n";
        std::abort();
    }
}

GLFWWrapper::~GLFWWrapper()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

void GLFWWrapper::swapBuffers()
{
    glfwSwapBuffers(window);
}

bool GLFWWrapper::shouldClose() const
{
    return glfwWindowShouldClose(window);
}

bool GLFWWrapper::getFramebufferSize(int &display_w, int &display_h) const
{
    if (!window)
    {
        return false;
    }

    glfwGetFramebufferSize(window, &display_w, &display_h);
    return true;
}