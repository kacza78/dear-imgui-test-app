## 3rd party libs

```bash
$ cd lib/
```
### GLFW
```bash
$ git clone https://github.com/glfw/glfw.git
$ cd glfw
$ mkdir build
$ cd build
$ cmake ..
$ make
```

### Dear ImGui
```bash
$ git clone https://github.com/ocornut/imgui.git
$ cd imgui
$ git checkout docking
```
