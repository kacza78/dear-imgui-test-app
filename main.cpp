#include <iostream>
#include <memory>

#include <ImGuiWrapper.hpp>

int main()
{
    std::shared_ptr<ImGuiWrapper> imgui = std::make_shared<ImGuiWrapper>();
    imgui->loop();

    std::cout << "Bye.\n";
}